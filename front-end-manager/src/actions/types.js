/**
 * In this file we save constant string that we use
 * purpose of this file is to help make less type errors
 */
//AUTH
export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";

//CREATE
export const CREATE_USER = "CREATE_USER";
export const CREATE_CLASS = "CREATE_CLASS";
export const CREATE_LESSON = "CREATE_LESSON";
export const CREATE_TEACHER = "CREATE_TEACHER";
export const CREATE_TIME = "CREATE_TIME";
export const CREATE_ROOM = "CREATE_ROOM";

//FETCH
export const FETCH_USER = "FETCH_USER";
export const FETCH_TEACHERS = "FETCH_TEACHERS";
export const FETCH_TIME = "FETCH_TIME";
export const FETCH_ROOMS = "FETCH_ROOMS";
export const FETCH_LESSONS = "FETCH_LESSONS";
export const FETCH_CLASSES = "FETCH_CLASSES";

//DELETE
export const DELETE_TEACHER = "DELETE_TEACHER";
export const DELETE_LESSON = "DELETE_LESSON";
export const DELETE_CLASS = "DELETE_CLASS";
export const DELETE_ROOM = "DELETE_ROOM";
export const DELETE_TIME = "DELETE_TIME";
export const DELETE_USER = "DELETE_USER";

//EDIT
export const EDIT_USER = "EDIT_USER";
