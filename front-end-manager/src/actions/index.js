import {
  CREATE_USER,
  EDIT_USER,
  DELETE_USER,
  FETCH_USER,
  FETCH_TEACHERS,
  FETCH_TIME,
  FETCH_ROOMS,
  FETCH_LESSONS,
  FETCH_CLASSES,
  LOGIN,
  LOGOUT,
  CREATE_ROOM,
  CREATE_CLASS,
  CREATE_LESSON,
  CREATE_TEACHER,
  CREATE_TIME,
  DELETE_CLASS,
  DELETE_LESSON,
  DELETE_TEACHER,
  DELETE_ROOM,
  DELETE_TIME,
} from "./types";
import users from "../apis/users";

//to create new user
export const createUser = (formValues) => async (dispatch) => {
  const response = await users.post("/users", formValues);
  dispatch({
    type: CREATE_USER,
    payload: response.data,
  });
};

//to get user info using user id
export const fetchUser = (id) => async (dispatch) => {
  const response = await users.get(`/users/${id}`);
  dispatch({ type: FETCH_USER, payload: response.data });
};

//to check if this user exists or not if yes then check authentications
export const loginUser = (formValues) => async (dispatch) => {
  const response = await users.post("/check", formValues);
  console.log("[login response]", response);
  if (response.status === 200) {
    localStorage.setItem("userId", response.data.id);
    localStorage.setItem("isSignedIn", true);
  }
  dispatch({ type: LOGIN, payload: response.data });
};

//to check if this user exists or not if yes then check authentications
export const logoutUser = () => async (dispatch) => {
  localStorage.setItem("userId", null);
  localStorage.setItem("isSignedIn", false);
  dispatch({ type: LOGOUT, payload: null });
};

// to edit user parameters
export const editUser = (id, formValues) => async (dispatch) => {
  const response = await users.put(`/users/${id}`, formValues);
  dispatch({ type: EDIT_USER, payload: response.data });
};

//CREATE ACTIONS
//to create new time
export const createTime = (formValues) => async (dispatch) => {
  let user_id = localStorage.getItem("userId");
  const response = await users.post(`/time/${user_id}`, formValues);
  dispatch({
    type: CREATE_TIME,
    payload: response.data,
  });
};

//to create new room
export const createRoom = (formValues) => async (dispatch) => {
  let user_id = localStorage.getItem("userId");
  const response = await users.post(`/rooms/${user_id}`, formValues);
  dispatch({
    type: CREATE_ROOM,
    payload: response.data,
  });
};

//to create new class
export const createClasss = (formValues) => async (dispatch) => {
  console.log("[create class]");
  let user_id = localStorage.getItem("userId");
  const response = await users.post(`/classes/${user_id}`, formValues);
  dispatch({
    type: CREATE_CLASS,
    payload: response.data,
  });
};

//to create new teacher
export const createTeacher = (formValues) => async (dispatch) => {
  let user_id = localStorage.getItem("userId");
  const response = await users.post(`/teachers/${user_id}`, formValues);
  dispatch({
    type: CREATE_TEACHER,
    payload: response.data,
  });
};

//to create new lesson
export const createLesson = (formValues) => async (dispatch) => {
  let user_id = localStorage.getItem("userId");
  const response = await users.post(`/lessons/${user_id}`, formValues);
  dispatch({
    type: CREATE_LESSON,
    payload: response.data,
  });
};

//FETCH

//to get time
export const fetchTime = () => async (dispatch) => {
  let user_id = localStorage.getItem("userId");
  const response = await users.get(`/time/${user_id}`);
  dispatch({ type: FETCH_TIME, payload: response.data });
};

//to get teachers list
export const fetchTeachers = () => async (dispatch) => {
  let user_id = localStorage.getItem("userId");
  const response = await users.get(`/teachers/${user_id}`);
  dispatch({ type: FETCH_TEACHERS, payload: response.data });
};

//to get rooms list
export const fetchRooms = () => async (dispatch) => {
  let user_id = localStorage.getItem("userId");
  const response = await users.get(`/rooms/${user_id}`);
  dispatch({ type: FETCH_ROOMS, payload: response.data });
};
//to get classes list
export const fetchClasses = () => async (dispatch) => {
  let user_id = localStorage.getItem("userId");
  const response = await users.get(`/classes/${user_id}`);
  dispatch({ type: FETCH_CLASSES, payload: response.data });
};
//to get lessons list
export const fetchLessons = () => async (dispatch) => {
  let user_id = localStorage.getItem("userId");
  const response = await users.get(`/lessons/${user_id}`);
  // if (response) console.log("[response lesson]", response);
  dispatch({ type: FETCH_LESSONS, payload: response.data });
};

//DELETE

//to delete user using user id
export const deleteUser = (id) => async (dispatch) => {
  await users.delete(`/users/${id}`);
  dispatch({ type: DELETE_USER, payload: id });
};

//to delete teacher by id
export const deleteTeacher = (id) => async (dispatch) => {
  await users.delete(`/teachers/${id}`);
  dispatch({ type: DELETE_TEACHER, payload: id });
};

//to delete class by id
export const deleteClass = (id) => async (dispatch) => {
  await users.delete(`/classes/${id}`);
  dispatch({ type: DELETE_CLASS, payload: id });
};

//to delete room by id
export const deleteRoom = (id) => async (dispatch) => {
  await users.delete(`/rooms/${id}`);
  dispatch({ type: DELETE_ROOM, payload: id });
};

//to delete lesson by id
export const deleteLesson = (id) => async (dispatch) => {
  await users.delete(`/lessons/${id}`);
  dispatch({ type: DELETE_LESSON, payload: id });
};

//to delete time table by id
export const deleteTime = (id) => async (dispatch) => {
  await users.delete(`/time/${id}`);
  dispatch({ type: DELETE_TIME, payload: id });
};
