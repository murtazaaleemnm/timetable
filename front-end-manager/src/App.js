import React, { Component } from "react";

import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import { connect } from "react-redux";
import 'bootstrap/dist/css/bootstrap.min.css';
//pages
import Main from "./components";
import PageNotFound from "./components/pages/js/PageNotFound";
import TimeTable from "./components/pages/js/TimeTable";
import Teachers from "./components/pages/js/Teachers";
import Classes from "./components/pages/js/Classes";
import Rooms from "./components/pages/js/Rooms";
import Lessons from "./components/pages/js/Lessons";
import Login from "./components/pages/js/Login";
import Signup from "./components/pages/js/Signup";

class App extends Component {
  render() {
    let { isSignedIn } = this.props;
    console.log("isSignedIn:", typeof isSignedIn);
    return (
      <Router>
        <Switch>
          {isSignedIn ? null : (
            <Route path='/' exact render={() => <Login />} />
          )}
          {isSignedIn ? null : (
            <Route path='/signup' exact render={() => <Signup />} />
          )}
          {isSignedIn ? (
            <Route
              path='/'
              exact
              render={() => <Main component={TimeTable} />}
            />
          ) : (
            <Redirect to='/login' />
          )}
          {isSignedIn ? (
            <Route
              path='/teachers'
              exact
              render={() => <Main component={Teachers} />}
            />
          ) : null}
          {isSignedIn ? (
            <Route
              path='/classes'
              exact
              render={() => <Main component={Classes} />}
            />
          ) : null}
          {isSignedIn ? (
            <Route
              path='/rooms'
              exact
              render={() => <Main component={Rooms} />}
            />
          ) : null}
          {isSignedIn ? (
            <Route
              path='/lessons'
              exact
              render={() => <Main component={Lessons} />}
            />
          ) : null}
          {isSignedIn ? (
            <Route
              path={["/", "/home"]}
              exact
              render={() => <Main component={TimeTable} />}
            />
          ) : null}
          <Route path='*' component={PageNotFound} />
        </Switch>
      </Router>
    );
  }
}

const mapStateToProps = (state) => {
  return { isSignedIn: state.auth.isSignedIn };
};
export default connect(mapStateToProps, null)(App);
