import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { Link } from "react-router-dom";

//imports for users api call
import { connect } from "react-redux";
import { createUser } from "../../../actions";

class Signup extends Component {
  renderError({ error, touched }) {
    if (touched && error) {
      return (
        <div className='ui error message'>
          <div className='header'> {error}</div>
        </div>
      );
    }
  }
  renderInput = ({ input, label, meta, type, placeholder }) => {
    const className = `field ${meta.error && meta.touched ? "error" : ""}`;
    return (
      <div className={className}>
        <input
          {...input}
          autoComplete='off'
          type={type}
          placeholder={placeholder}
        />
        {this.renderError(meta)}
      </div>
    );
  };

  onSubmit = (formValues) => {
    console.log(formValues);
    this.props.createUser(formValues);
  };
  render() {
    console.log(this.props);
    return (
      <div
        className='ui middle aligned center aligned grid'
        style={{ height: "-webkit-fill-available" }}
      >
        <div className='five wide column'>
          <h2 className='ui blue header'>
            <div className='content'>Create new account</div>
          </h2>
          <form
            onSubmit={this.props.handleSubmit(this.onSubmit)}
            className='ui large form error'
          >
            <Field
              name='username'
              component={this.renderInput}
              type='text'
              placeholder='E-mail address'
            />
            <Field
              name='passwordN'
              component={this.renderInput}
              type='password'
              placeholder='Password'
            />
            <Field
              name='passwordC'
              component={this.renderInput}
              type='password'
              placeholder='Re-enter password'
            />
            <button className='ui fluid large blue submit button'>
              Signup
            </button>
          </form>

          <div className='ui message'>
            Have an account? <Link to='/'>Login</Link>
          </div>
        </div>
      </div>
    );
  }
}

const validate = (formValues) => {
  const errors = {};

  if (!formValues.username) {
    //only ran if user not entered username
    errors.username = "you must enter username ";
  }

  if (!formValues.passwordN) {
    //only ran if user not entered password
    errors.passwordN = "you must enter password ";
  }
  if (!formValues.passwordC) {
    //only ran if user not entered password
    errors.passwordC = "you must enter password ";
  }
  return errors;
};

const formWrapped = reduxForm({
  form: "signup",
  validate,
})(Signup);

export default connect(null, { createUser })(formWrapped);
