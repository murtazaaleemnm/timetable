import React, { Component } from "react";
import Tabs from "../../tabs/teachers";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import "../css/Teachers.css";
//imports for teachers api call
import { connect } from "react-redux";
import { fetchTeachers, fetchLessons, deleteTeacher } from "../../../actions";
import CreateTeacherForm from "../../forms/teachers/CreateTeacherForm";
import { withTranslation } from "react-i18next";
class Teachers extends Component {
  componentDidMount() {
    this.props.fetchTeachers();
    this.props.fetchLessons();
  }
  render() {
    // const { classes } = this.props;
    return (
      <div>
        <Grid container spacing={3}>
          <Grid item xs={9}>
            <Tabs {...this.props} />
          </Grid>
          <Grid item xs={3}>
            <Paper>
              <CreateTeacherForm {...this.props} />
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    teachers: Object.values(state.teacher),
    lessons: Object.values(state.lesson),
  };
};

const wrapped = connect(mapStateToProps, {
  fetchTeachers,
  fetchLessons,
  deleteTeacher,
})(Teachers);

export default withTranslation()(wrapped);
