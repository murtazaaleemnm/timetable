import React, { Component } from "react";
import Tabs from "../../tabs/rooms";
import NewCreateRoomForm from "../../forms/rooms/NewCreateRoomForm";

import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/styles";
import "../css/Rooms.css";
//imports for teachers api call
import { connect } from "react-redux";
import { fetchRooms, deleteRoom } from "../../../actions";
import { withTranslation } from "react-i18next";
class Rooms extends Component {
  componentDidMount() {
    this.props.fetchRooms();
  }
  render() {
    return (
      <div>
        <Grid container spacing={3}>
          <Grid item xs={9}>
            <Tabs {...this.props} />
          </Grid>
          <Grid item xs={3}>
            <Paper>
              <NewCreateRoomForm {...this.props} />
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { rooms: Object.values(state.room) };
};

const wrapped = connect(mapStateToProps, { fetchRooms, deleteRoom })(Rooms);

export default withTranslation()(wrapped);
