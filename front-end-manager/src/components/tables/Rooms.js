import React, { Component } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";

class SimpleTable extends Component {
  render() {
    let { rooms, deleteRoom, t } = this.props;
    console.log("{rooms table}", rooms);
    return (
      <TableContainer component={Paper}>
        <Table aria-label='simple table'>
          <TableHead>
            <TableRow>
              <TableCell>{t("table-fields.rooms-table.room-number")}</TableCell>
              <TableCell align='right'>{t("table-fields.rooms-table.room-type")}</TableCell>
              <TableCell align='right'>{t("table-fields.rooms-table.room-capacity")}</TableCell>
              <TableCell align='right'>{t("table-fields.delete")}</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rooms.length > 0 && rooms[0]
              ? rooms[0].length > 0
                ? rooms[0].map((room) => (
                    <TableRow key={room.number}>
                      <TableCell component='th' scope='row'>
                        {room.name}
                      </TableCell>
                      <TableCell align='right'>{room.type}</TableCell>
                      <TableCell align='right'>{room.capacity}</TableCell>
                      <TableCell align='right'>
                        {" "}
                        <Button
                          variant='contained'
                          color='secondary'
                          onClick={() => deleteRoom(room._id)}
                        >
                          {t("Button.delete")}
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))
                : null
              : null}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
}

export default SimpleTable;
