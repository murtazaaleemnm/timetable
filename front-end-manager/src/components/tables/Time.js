import React, { Component } from "react";
import Table from "react-bootstrap/Table";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
class SimpleTable extends Component {
  returnColor(class_name) {
    let { classes } = this.props;
    let color = null;
    if (classes && classes[0] && classes[0].length > 0) {
      classes[0].map((classs) => {
        if (classs.name === class_name) {
          color = classs.color_code;
        }
      });
    }
    return color;
  }
  returnLessonName(lesson_id) {
    let { lessons } = this.props;
    let lesson_name = null;
    if (lessons && lessons[0] && lessons[0].length > 0) {
      lessons[0].map((lesson) => {
        if (lesson._id === lesson_id) {
          lesson_name = lesson.name;
        }
      });
    }
    return lesson_name;
  }
  renderCell(pars) {
    return (
      <>
        {pars.map((par) => {
          return (
            <td style={{ backgroundColor: this.returnColor(par.classs) }}>
              <div>{par.classs}</div>
              <div>{par.room}</div>
              <div>{this.returnLessonName(par.lesson)}</div>
            </td>
          );
        })}
      </>
    );
  }
  returnClass(teacher_code) {
    let class_name = null;
    this.props.classes[0].map((classs) => {
      if (classs.teacher_code === teacher_code) {
        class_name = classs.name;
      }
    });
    return class_name;
  }
  renderRow(teachers) {
    return (
      <>
        {teachers.map((teacher) => {
          let info = teacher.teacher;
          let pars = teacher.pars;
          return (
            <tr>
              <td>{info.name}</td>
              <td>{this.returnClass(info.code)}</td>
              {this.renderCell(pars)}
            </tr>
          );
        })}
      </>
    );
  }
  renderTimeTable(week) {
    let { t } = this.props;
    return (
      <>
        {week &&
          week[0] &&
          week[0].week &&
          week[0].week.map((day) => {
            return (
              <TabPanel>
                <Table responsive='sm'>
                  <thead>
                    <tr>
                      <th>{t("table-fields.time-table.teacher")}</th>
                      <th>{t("table-fields.time-table.class")}</th>
                      <th>{t("table-fields.time-table.par")}1-1</th>
                      <th>{t("table-fields.time-table.par")}1-2</th>
                      <th>{t("table-fields.time-table.par")}1-3</th>
                      <th>{t("table-fields.time-table.par")}1-4</th>
                      <th>{t("table-fields.time-table.par")}1-5</th>
                      <th>{t("table-fields.time-table.par")}1-6</th>
                      <th>{t("table-fields.time-table.par")}1-7</th>
                      <th>{t("table-fields.time-table.par")}2-1</th>
                      <th>{t("table-fields.time-table.par")}2-2</th>
                      <th>{t("table-fields.time-table.par")}2-3</th>
                      <th>{t("table-fields.time-table.par")}2-4</th>
                      <th>{t("table-fields.time-table.par")}2-5</th>
                      <th>{t("table-fields.time-table.par")}2-6</th>
                    </tr>
                  </thead>
                  <tbody>{this.renderRow(day.teachers)}</tbody>
                </Table>
              </TabPanel>
            );
          })}
      </>
    );
  }
  render() {
    let { time, t, classes } = this.props;

    return (
      <div>
        <Tabs>
          <TabList>
            <Tab>{t("tabs-titles.monday")}</Tab>
            <Tab>{t("tabs-titles.tuesday")}</Tab>
            <Tab>{t("tabs-titles.wednesday")}</Tab>
            <Tab>{t("tabs-titles.thursday")}</Tab>
            <Tab>{t("tabs-titles.friday")}</Tab>
            <Tab>{t("tabs-titles.saturday")}</Tab>
            <Tab>{t("tabs-titles.sunday")}</Tab>
          </TabList>

          {time && time[0] && classes && classes[0]
            ? this.renderTimeTable(time[0])
            : null}
        </Tabs>
      </div>
    );
  }
}

export default SimpleTable;
