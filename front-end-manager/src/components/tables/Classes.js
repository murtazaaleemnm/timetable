import React, { Component } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
class SimpleTable extends Component {
  render() {
    let { classess, deleteClass, t } = this.props;
    console.log("[table classes]", classess);
    return (
      <TableContainer component={Paper}>
        <Table aria-label='simple table'>
          <TableHead>
            <TableRow>
              <TableCell>{t("table-fields.class-table.class-name")}</TableCell>
              <TableCell align='right'>
                {t("table-fields.class-table.teacher-code")}
              </TableCell>
              <TableCell align='right'>
                {t("table-fields.class-table.students-count")}
              </TableCell>
              <TableCell align='right'>
                {t("table-fields.class-table.room-number")}
              </TableCell>
              <TableCell align='right'>{t("table-fields.delete")}</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {classess && classess[0]
              ? classess[0].map((classs) => (
                  <TableRow key={classs.name}>
                    <TableCell component='th' scope='row'>
                      {classs.name}
                    </TableCell>
                    <TableCell align='right'>{classs.teacher_code}</TableCell>
                    <TableCell align='right'>{classs.students_count}</TableCell>
                    <TableCell align='right'>{classs.room_number}</TableCell>
                    <TableCell align='right'>
                      {" "}
                      <Button
                        variant='contained'
                        color='secondary'
                        onClick={() => deleteClass(classs._id)}
                      >
                        {t("Button.delete")}
                      </Button>
                    </TableCell>
                  </TableRow>
                ))
              : null}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
}

export default SimpleTable;
