import React, { Component } from "react";

//imports for users api call
import { connect } from "react-redux";
import { createClasss } from "../../../actions";

class CreateClass extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: "high",
      studentsCount: null,
      name: null,
      teacherCode: null,
      roomNumber: null,
      must_lessons: [],
      selectedLesson: null,
      selectedTime: 1,
      color_code: "#ff0000",
    };
    this.handleChangeSelectedColor = this.handleChangeSelectedColor.bind(this);
    this.handleChangeSelectedTime = this.handleChangeSelectedTime.bind(this);
    this.handleChangeSelectedLesson = this.handleChangeSelectedLesson.bind(
      this
    );
    this.handleChangeRoomNumber = this.handleChangeRoomNumber.bind(this);
    this.handleChangeTeacherCode = this.handleChangeTeacherCode.bind(this);
    this.handleChangeStudentsCount = this.handleChangeStudentsCount.bind(this);
    this.handleChangeType = this.handleChangeType.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChangeSelectedColor(event) {
    this.setState({ color_code: event.target.value });
  }
  handleChangeSelectedTime(event) {
    this.setState({ selectedTime: event.target.value });
  }
  handleChangeSelectedLesson(event) {
    this.setState({ selectedLesson: event.target.value });
  }
  handleChangeRoomNumber(event) {
    this.setState({ roomNumber: event.target.value });
  }
  handleChangeTeacherCode(event) {
    this.setState({ teacherCode: event.target.value });
  }
  handleChangeStudentsCount(event) {
    this.setState({ studentsCount: event.target.value });
  }
  handleChangeName(event) {
    this.setState({ name: event.target.value });
  }
  handleChangeType(event) {
    this.setState({ type: event.target.value });
  }

  addMustLesson(event) {
    event.preventDefault();

    let lesson = {
      id: this.state.selectedLesson,
      time: parseInt(this.state.selectedTime),
    };
    let lessons = this.state.must_lessons;
    lessons.push(lesson);
    this.setState({
      must_lessons: lessons,
    });
    console.log(lessons);
  }
  handleSubmit(event) {
    alert("Шинэ анги үүслээ");
    let values = {
      name: this.state.name,
      type: this.state.type,
      students_count: this.state.studentsCount,
      teacher_code: this.state.teacherCode,
      room_number: this.state.roomNumber,
      must_lessons: this.state.must_lessons,
      color_code: this.state.color_code,
    };
    console.log("[values]", values);
    this.props.createClasss(values);
  }

  returnLessonName(Rlesson) {
    let { lessons } = this.props;
    let name = "";
    console.log("[lessons]", lessons);
    lessons[0].map((lesson) => {
      if (lesson._id === Rlesson.id) {
        name = name + lesson.name + " " + Rlesson.time;
      }
    });
    console.log("[lesson]", Rlesson);
    return name;
  }
  componentDidUpdate() {
    console.log("[state]", this.state);
  }
  render() {
    let { teachers, rooms, lessons, t } = this.props;
    return (
      <form className='form' onSubmit={this.handleSubmit}>
        <div className=' form-group'>
          <label> {t("form.label.class-name")}:</label>
          <input
            className='form-control'
            type='text'
            value={this.state.name}
            onChange={this.handleChangeName}
            placeholder={t("form.placeholder.class-name")}
          />
        </div>
        <div className=' form-group'>
          <label> {t("form.label.class-type")}:</label>
          <select
            className='form-control'
            value={this.state.type}
            onChange={this.handleChangeType}
          >
            <option value='high'>Smart Class</option>
            <option value='low'>Normal</option>
          </select>
        </div>
        <div className=' form-group'>
          <label>{t("form.label.class-students-count")}:</label>
          <input
            type='nummber'
            className='form-control'
            value={this.state.studentsCount}
            onChange={this.handleChangeStudentsCount}
            placeholder={t("form.placeholder.class-students-count")}
          />
        </div>
        <div className=' form-group'>
          <label> {t("form.label.class-teacher")}:</label>
          <select
            className='form-control'
            value={this.state.teacherCode}
            onChange={this.handleChangeTeacherCode}
            placeholder={t("form.placeholder.class-teacher")}
          >
            <option>...</option>
            {teachers && teachers[0]
              ? teachers[0].map((teacher) => {
                  return <option value={teacher.code}>{teacher.name}</option>;
                })
              : null}
          </select>
        </div>
        <div className=' form-group'>
          <label>{t("form.label.room-number")}:</label>
          <select
            className='form-control'
            value={this.state.roomNumber}
            onChange={this.handleChangeRoomNumber}
            placeholder={t("form.placeholder.class-room-number")}
          >
            <option>...</option>
            {rooms && rooms[0]
              ? rooms[0].map((room) => {
                  return <option value={room.name}>{room.name}</option>;
                })
              : null}
          </select>
        </div>
        <div className=' form-group'>
          <label> {t("form.label.class-lessons")}:</label>
          <select
            className='form-control'
            value={this.state.selectedLesson}
            onChange={this.handleChangeSelectedLesson}
          >
            <option>...</option>
            {lessons && lessons[0]
              ? lessons[0].map((lesson) => {
                  return <option value={lesson._id}>{lesson.name}</option>;
                })
              : null}
          </select>

          <input
            type='nummber'
            className='form-control'
            value={this.state.selectedTime}
            onChange={this.handleChangeSelectedTime}
          />
          {this.state.must_lessons && this.state.must_lessons.length > 0 ? (
            <ul>
              {this.state.must_lessons.map((lesson) => {
                return <li>{this.returnLessonName(lesson)}</li>;
              })}
            </ul>
          ) : null}
          <button
            className='form-control'
            onClick={(event) => this.addMustLesson(event)}
          >
            add
          </button>
        </div>
        <div className=' form-group'>
          <label>{t("form.label.class-color")}:</label>
          <input
            type='color'
            className='form-control'
            value={this.state.color_code}
            onChange={this.handleChangeSelectedColor}
            placeholder={t("form.placeholder.class-color")}
          />
        </div>
        <div className='d-flex button' style={{ justifyContent: "center" }}>
          <button type='submit' class='btn btn-outline-primary'>
            {t("Button.create")}
          </button>
        </div>
      </form>
    );
  }
}

export default connect(null, { createClasss })(CreateClass);
