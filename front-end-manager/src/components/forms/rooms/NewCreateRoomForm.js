import React, { Component } from "react";

//imports for users api call
import { connect } from "react-redux";
import { createRoom } from "../../../actions";

class CreateRoom extends Component {
  constructor(props) {
    super(props);
    this.state = { type: "N", capacity: null, name: "" };

    this.handleChangeCapacity = this.handleChangeCapacity.bind(this);
    this.handleChangeType = this.handleChangeType.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangeCapacity(event) {
    this.setState({ capacity: event.target.value });
  }
  handleChangeName(event) {
    this.setState({ name: event.target.value });
  }
  handleChangeType(event) {
    this.setState({ type: event.target.value });
  }
  handleSubmit(event) {
    alert("Шинэ өрөө үүслээ");
    let values = {
      roomNumber: this.state.name,
      type: this.state.type,
      capacity: this.state.capacity,
    };
    this.props.createRoom(values);
  }
  render() {
    let { t } = this.props;
    return (
      <form className='form' onSubmit={this.handleSubmit}>
        <div className=' form-group'>
          <label>{t("form.label.room-number")}:</label>
          <input
            type='text'
            className='form-control'
            value={this.state.name}
            onChange={this.handleChangeName}
            placeholder={t("form.placeholder.room-number")}
          />
        </div>
        <div className=' form-group'>
          <label>{t("form.label.room-type")}:</label>
          <select
            className='form-control'
            value={this.state.type}
            onChange={this.handleChangeType}
          >
            <option value='N'>Listening</option>
            <option value='P'>Normal</option>
            {/* <option value='C'>Хими</option>
            <option value='M'>Хөгжим</option>
            <option value='Z'>Заал</option> */}
          </select>
        </div>
        <div className=' form-group'>
          <label>{t("form.label.room-capacity")}:</label>
          <input
            type='nummber'
            className='form-control'
            value={this.state.capacity}
            onChange={this.handleChangeCapacity}
            placeholder={t("form.placeholder.room-capacity")}
          />
        </div>
        <div className='d-flex button' style={{ justifyContent: "center" }}>
          <button type='submit' class='btn btn-outline-primary'>
            {t("Button.create")}
          </button>
        </div>
      </form>
    );
  }
}

export default connect(null, { createRoom })(CreateRoom);
