import React, { Component } from "react";
import MultiSelect from "react-multi-select-component";
//imports for users api call
import { connect } from "react-redux";
import { createTeacher } from "../../../actions";
class CreateTeacher extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: "",
      name: "",
      age: null,
      lessons: null,
      options: null,
      grade: "high",
    };
    this.handleChangeGrade = this.handleChangeGrade.bind(this);
    this.handleChangeLessons = this.handleChangeLessons.bind(this);
    this.handleChangeCode = this.handleChangeCode.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangeAge = this.handleChangeAge.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  renderOptions(lessons) {
    let temp = [];
    lessons.forEach((element) => {
      let item = {
        label: element.name,
        value: element,
      };
      temp.push(item);
    });
    this.setState({ options: temp });
  }
  handleChangeName(event) {
    this.setState({ name: event.target.value });
  }
  handleChangeCode(event) {
    this.setState({ code: event.target.value });
  }
  handleChangeAge(event) {
    this.setState({ age: event.target.value });
  }
  handleChangeGrade(event) {
    this.setState({ grade: event.target.value });
  }
  handleSubmit(event) {
    alert("Шинэ багш үүслээ");
    let values = {
      code: this.state.code,
      name: this.state.name,
      age: this.state.age,
      lessons: this.state.lessons,
      grade: this.state.grade,
    };
    this.props.createTeacher(values);
  }
  handleChangeLessons(value) {
    this.setState({ lessons: value });
  }

  render() {
    let { lessons, t } = this.props;
    if (this.state.options === null) {
      if (lessons) {
        if (lessons[0]) {
          this.renderOptions(lessons[0]);
        }
      }
    }
    return (
      <form className='form' onSubmit={this.handleSubmit}>
        <div className=' form-group'>
          <label>{t("form.label.teacher-code")}:</label>
          <input
            type='text'
            className='form-control'
            value={this.state.code}
            onChange={this.handleChangeCode}
            placeholder={t("form.placeholder.teacher-code")}
          />
        </div>
        <div className=' form-group'>
          <label>{t("form.label.teacher-name")}:</label>
          <input
            type='text'
            className='form-control'
            value={this.state.name}
            onChange={this.handleChangeName}
            placeholder={t("form.placeholder.teacher-name")}
          />
        </div>
        {/* <div className=' form-group'>
          <label>{t("form.label.teacher-age")}:</label>
          <input
            type='number'
            className='form-control'
            value={this.state.age}
            onChange={this.handleChangeAge}
            placeholder={t("form.placeholder.teacher-age")}
          />
        </div> */}
        <div className=' form-group'>
          <label>{t("form.label.teacher-grade")}:</label>
          <select
            className='form-control'
            value={this.state.grade}
            onChange={this.handleChangeGrade}
          >
            <option value='high'>Intermediate</option>
            <option value='low'>Basic</option>
          </select>
        </div>
        <div className=' form-group'>
          <label>{t("form.label.teacher-lessons")}:</label>
          <div className='col'>
            {this.state.options && (
              <MultiSelect
                options={this.state.options}
                value={this.state.lessons}
                onChange={this.handleChangeLessons}
                labelledBy={"Select"}
              />
            )}
          </div>
        </div>
        <div className='d-flex button' style={{ justifyContent: "center" }}>
          <button type='submit' class='btn btn-outline-primary'>
            {t("Button.create")}
          </button>
        </div>

        {/* <input type='submit' value='Submit' /> */}
      </form>
    );
  }
}

export default connect(null, { createTeacher })(CreateTeacher);
