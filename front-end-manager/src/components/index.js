import React, { Component } from "react";
import Dashboard from "./layouts";

class Main extends Component {
  render() {
    console.log("main :", this.props);
    return (
      <>
        <Dashboard>
          <this.props.component />
        </Dashboard>
      </>
    );
  }
}

export default Main;
