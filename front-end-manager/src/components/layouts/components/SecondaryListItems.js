import React from "react";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { connect } from "react-redux";
import { logoutUser } from "../../../actions";

//design import
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";
import AssignmentIcon from "@material-ui/icons/Assignment";

const SecondaryListItems = (props) => {
  const { t } = useTranslation();
  return (
    <div>
      <ListSubheader inset>{t("SideBar.Settings")}</ListSubheader>
      <Link to='/' onClick={props.logoutUser}>
        <ListItem button>
          <ListItemIcon>
            <AssignmentIcon />
          </ListItemIcon>
          <ListItemText primary={t("SideBar.Logout")} />
        </ListItem>
      </Link>
    </div>
  );
};

export default connect(null, { logoutUser })(SecondaryListItems);
