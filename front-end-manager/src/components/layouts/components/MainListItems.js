import React from "react";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

//design import
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import DashboardIcon from "@material-ui/icons/Dashboard";
import PeopleIcon from "@material-ui/icons/People";
import BarChartIcon from "@material-ui/icons/BarChart";
import LayersIcon from "@material-ui/icons/Layers";

export default function MainListItems() {
  const { t } = useTranslation();
  return (
    <div>
      <Link to='/'>
        <ListItem button>
          <ListItemIcon>
            <DashboardIcon />
          </ListItemIcon>
          <ListItemText primary={t("SideBar.TimeTable")} />
        </ListItem>
      </Link>
      <Link to='/teachers'>
        <ListItem button>
          <ListItemIcon>
            <PeopleIcon />
          </ListItemIcon>
          <ListItemText primary={t("SideBar.Teachers")} />
        </ListItem>
      </Link>
      <Link to='/classes'>
        <ListItem button>
          <ListItemIcon>
            <PeopleIcon />
          </ListItemIcon>
          <ListItemText primary={t("SideBar.Classes")} />
        </ListItem>
      </Link>
      <Link to='/rooms'>
        <ListItem button>
          <ListItemIcon>
            <BarChartIcon />
          </ListItemIcon>
          <ListItemText primary={t("SideBar.Rooms")} />
        </ListItem>
      </Link>
      <Link to='/lessons'>
        <ListItem button>
          <ListItemIcon>
            <LayersIcon />
          </ListItemIcon>
          <ListItemText primary={t("SideBar.Lessons")} />
        </ListItem>
      </Link>
    </div>
  );
}
