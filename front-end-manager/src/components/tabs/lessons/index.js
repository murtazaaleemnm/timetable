import React, { Component } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import Lessons from "../../tables/Lessons";
import "react-tabs/style/react-tabs.css";
//design import
import { withTranslation } from "react-i18next";
class LessonTabs extends Component {
  render() {
    let { t } = this.props;
    return (
      <Tabs>
        <TabList>
          <Tab>{t("tabs-titles.lessons-list")}</Tab>
        </TabList>

        <TabPanel>
          <Lessons {...this.props} />
        </TabPanel>
      </Tabs>
    );
  }
}

export default withTranslation()(LessonTabs);
