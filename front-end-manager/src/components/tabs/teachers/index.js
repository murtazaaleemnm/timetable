import React, { Component } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import Teachers from "../../tables/Teachers";
import "react-tabs/style/react-tabs.css";
//design import
import { withTranslation } from "react-i18next";
class TeacherTabs extends Component {
  render() {
    let { t } = this.props;
    return (
      <Tabs>
        <TabList>
          <Tab>{t("tabs-titles.teachers-list")}</Tab>
        </TabList>

        <TabPanel>
          <Teachers {...this.props} />
        </TabPanel>
      </Tabs>
    );
  }
}

export default withTranslation()(TeacherTabs);
