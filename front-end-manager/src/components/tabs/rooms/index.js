import React, { Component } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import Rooms from "../../tables/Rooms";
import "react-tabs/style/react-tabs.css";
//design import
import { withTranslation } from "react-i18next";
class RoomTabs extends Component {
  render() {
    let { t } = this.props;
    return (
      <Tabs>
        <TabList>
          <Tab>{t("tabs-titles.rooms-list")}</Tab>
        </TabList>

        <TabPanel>
          <Rooms {...this.props} />
        </TabPanel>
      </Tabs>
    );
  }
}

export default withTranslation()(RoomTabs);
