import { LOGIN, LOGOUT } from "../actions/types";

const INITIAL_STATE = {
  isSignedIn: localStorage.getItem("isSignedIn")
    ? JSON.parse(localStorage.getItem("isSignedIn"))
    : false,
  userId: localStorage.getItem("userId")
    ? localStorage.getItem("userId")
    : null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    // when login action calls state isSignedIn changes to true
    case LOGIN:
      return { ...state, isSignedIn: true, userId: action.payload.id };
    // when logout action calls state isSignedIn changes to false
    case LOGOUT:
      return { ...state, isSignedIn: false, userId: null };
    default:
      return state;
  }
};
