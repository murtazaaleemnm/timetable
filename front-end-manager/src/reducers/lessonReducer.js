import _ from "lodash";
import { CREATE_LESSON, FETCH_LESSONS, DELETE_LESSON } from "../actions/types";

export default (state = {}, action) => {
  switch (action.type) {
    case CREATE_LESSON:
      return { ...state, [action.payload.id]: action.payload };
    case FETCH_LESSONS:
      return { ...state, [action.payload.id]: action.payload };
    case DELETE_LESSON:
      //omit n shine state uusgej bga ba action payload deer irsen id tai hereglegch ustah um
      return _.omit(state, action.payload);
    default:
      return state;
  }
};
