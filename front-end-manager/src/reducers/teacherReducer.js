import _ from "lodash";
import {
  CREATE_TEACHER,
  FETCH_TEACHERS,
  DELETE_TEACHER,
} from "../actions/types";

export default (state = {}, action) => {
  switch (action.type) {
    case CREATE_TEACHER:
      return { ...state, [action.payload.id]: action.payload };
    case FETCH_TEACHERS:
      return { ...state, [action.payload.id]: action.payload };
    case DELETE_TEACHER:
      //omit n shine state uusgej bga ba action payload deer irsen id tai hereglegch ustah um
      return _.omit(state, action.payload);
    default:
      return state;
  }
};
