const mongoose = require("mongoose");

const parSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  period: Number,
  activity: Array, 
});

module.exports = mongoose.model("Par", parSchema);