const mongoose = require("mongoose");

const activitySchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  teacher: Object,
  classs: Object,
  room: Object,
  lesson: Object,
});

module.exports = mongoose.model("Activity", activitySchema);
