const mongoose = require("mongoose");

const timeSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  user_id: String,
  par_count: Number,
  week: Array,
});

module.exports = mongoose.model("Time", timeSchema);
