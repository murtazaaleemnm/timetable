const mongoose = require("mongoose");

const classSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  user_id: String,
  room_number: String,
  teacher_code: String,
  type: String,
  name: String,
  students_count: Number,
  must_lessons: Array,
  color_code:String,
});

module.exports = mongoose.model("Class", classSchema);
