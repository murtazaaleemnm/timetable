const mongoose = require("mongoose");

const lessonSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  user_id: String,
  name: String,
  code: String
});

module.exports = mongoose.model("Lesson", lessonSchema);
