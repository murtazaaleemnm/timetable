const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const Class = require("../models/class");
const _ = require("lodash");

//GET request huleen avah
router.get("/:userId", (req, res, next) => {
  Class.find()
    .exec()
    .then((docs) => {
      console.log(docs);
      if (docs.length >= 0) {
        let grouped = _.groupBy(docs, "user_id");
        res.status(200).json(grouped[req.params.userId]);
      } else {
        res.status(404).json({
          message: "No entries found",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});
//POST request huleen avah
router.post("/:userId", (req, res, next) => {
  //doorh n shine object uusgeed tuund utguudiig onoon yvuulna
  const classs = new Class({
    _id: mongoose.Types.ObjectId(),
    user_id: req.params.userId,
    name: req.body.name,
    type: req.body.type,
    students_count: req.body.students_count,
    teacher_code: req.body.teacher_code,
    room_number: req.body.room_number,
    must_lessons: req.body.must_lessons,
    color_code: req.body.color_code,
  });
  classs
    .save()
    .then((result) => {
      console.log(result);
    })
    .catch((err) => console.log(err));
  res.status(201).json({
    message: "Handling POST request to /classes",
    createdClass: classs,
  });
});

router.get("/:userId/:classId", (req, res, next) => {
  const id = req.params.classId;
  Class.findById(id)
    .exec()
    .then((doc) => {
      console.log(doc);
      if (doc) {
        res.status(200).json(doc);
      } else {
        res.status(404).json({
          message: "no valid id found",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

router.patch("/:classId", (req, res, next) => {
  const id = req.params.classId;
  const updateOps = {};
  //request yvyylahda [{"propName":"name", "value":"new value"}] gesen zagvaraar yvuulah
  //doorh for functs tuslamjtaigaar umar ch turliin update avah bolomjtoi bolno
  for (const ops of req.body) {
    updateOps[ops.propName] = ops.value;
  }
  //update functs n uurchluh gj bga object iin id-g avaad uurchluh field iin avna
  Class.update({ _id: id }, { $set: updateOps })
    .exec()
    .then((result) => {
      console.log(result);
      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

router.delete("/:classId", (req, res, next) => {
  const id = req.params.classId;
  Class.remove({ _id: id })
    .exec()
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

module.exports = router;
