const express = require("express");
const app = express();
//uses to connect mongodb
const mongoose = require("mongoose");
//midle ware to handle error or something
const morgan = require("morgan");
//json ynzlah
const bodyParser = require("body-parser");

//api-uud importolj oruulj irj bn
const teacherRoutes = require("./api/routes/teachers");
const classRoutes = require("./api/routes/classes");
const userRoutes = require("./api/routes/users");
const loginRoutes = require("./api/routes/login");
const roomRoutes = require("./api/routes/rooms");
const lessonRoutes = require("./api/routes/lessons");
const timeRoutes = require("./api/routes/time");

//mongoDB luu handhad ashiglah URL hayg (username , password ) hereglegch burt ylgaatai
const URI = `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@node-rest-data-rlof6.mongodb.net/${process.env.MONGO_DB}?retryWrites=true&w=majority`;

//mongoose ashiglan holbolt uusgej baina
mongoose.connect(URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

/**=====================================
 * midle wires start
 * =====================================
 */
app.use(morgan("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
/**=====================================
 * midle wires start
 * =====================================
 */

//prevent cors errors
//CORS policy n cross site attack-aas sergiilne
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});


//routes should handle request
app.use("/lessons", lessonRoutes);
app.use("/rooms", roomRoutes);
app.use("/teachers", teacherRoutes);
app.use("/classes", classRoutes);
app.use("/users", userRoutes);
app.use("/check", loginRoutes);
app.use("/time", timeRoutes);


//routes dund oldoogui ued
app.use((req, res, next) => {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});


//server faildeh ued zaah aldaa
app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message,
    },
  });
});
module.exports = app;
